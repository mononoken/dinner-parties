class User < ApplicationRecord
  validates :name, presence: true, uniqueness: true

  has_many :parties, foreign_key: 'host_id', inverse_of: 'host'
  has_many :invitations, inverse_of: :guest
end
