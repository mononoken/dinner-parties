class Party < ApplicationRecord
  validates :name, presence: true, uniqueness: true

  has_many :invitations
  has_many :guests, class_name: 'User', through: :invitations
  belongs_to :host, class_name: 'User'

  def accepted_guests
    accepted_invitations.map(&:guest)
  end

  private

  def accepted_invitations
    invitations.select { |invitation| invitation.accepted == true }
  end
end
