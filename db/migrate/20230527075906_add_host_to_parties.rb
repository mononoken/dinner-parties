class AddHostToParties < ActiveRecord::Migration[7.0]
  def change
    add_reference :parties, :host, null: false, foreign_key: { to_table: :users }
  end
end
