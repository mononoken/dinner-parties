class AddPartyToInvitations < ActiveRecord::Migration[7.0]
  def change
    add_reference :invitations, :party, null: false, foreign_key: true
  end
end
