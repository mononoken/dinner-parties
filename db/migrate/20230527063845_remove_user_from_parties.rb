class RemoveUserFromParties < ActiveRecord::Migration[7.0]
  def change
    remove_reference :parties, :user, null: false, foreign_key: true,
                                      index: { name: 'host_id' }
  end
end
